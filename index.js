document.getElementById('container').innerHTML = '<div class="row mt-5">' +
    '<div class="col-md-4">' + '<div class="card bg-warning text-white">' +
    '<div class="card-header">' + '<h1> Add Car </h1>' + '</div>' +
    '<div class="card-body">' +
    '<form name="carForm">' +
    '<input class="form-control mb-3" name="countryName" type="text" required placeholder="Country Name">' +
    '<input class="form-control mb-3" name="stateName" type="text" required placeholder="State Name">' +
    '<input class="form-control mb-3" name="carComName" type="text" required placeholder="Car Company Name">' +
    '<input class="form-control mb-3" name="carYear" type="number" required placeholder="Car Year">' +
    '<input class="form-control mb-3" name="setTime" type="datetime-local" required placeholder="Set Time">' +
    '<input class="form-control mb-3" name="clear" type="reset" value="Clear">' +
    '</form>' +
    '</div>' +
    '<div class="card-footer">' +
    '<button class="btn btn-outline-light d-block w-100" onclick="createList()">Add to List</button>' +
    '</div>' +
    '</div>' + '</div>' +
    '<div class="col-md-8">' +
    '<table class="table table-striped table-bordered">' +
    '<thead class="text-center text-white bg-dark">' +
    '<tr>' +
    '<th id="dNomi">Davlat Nomi</th>' +
    '<th id="pNomi">Poytaxt nomi</th>' +
    '<th id="mkNomi">Mashina Kompaniyasi Nomi</th>' +
    '<th id="mYili">Mashina Yili</th>' +
    '<th id="qVaqti">Qo\'shilgan Vaqti</th>' +
    '<th id="editUser">Edit</th>' +
    '<th id="deleteUser">Delelte User</th>' +
    '</tr>' +
    '</thead>' +
    '<tbody class="text-center text-dark bg-warning " id="tBody">' +
    '</tbody>' +
    '</table>' +
    '</div>' +
    '</div>';


let cars = [];
let edited = -1;

function createList() {
    let dNomi = document.forms['carForm']['countryName'].value;
    let pNomi = document.forms['carForm']['stateName'].value;
    let mkNomi = document.forms['carForm']['carComName'].value;
    let mYili = document.forms['carForm']['carYear'].value;
    let sTime = document.forms['carForm']['setTime'].value;
    if (dNomi.trim().length > 0 && sTime.trim().length > 0 && mYili.trim().length > 0 && mkNomi.trim().length > 0 && pNomi.trim().length > 0) {

        let newCar = {
            dNomi: dNomi,
            pNomi: pNomi,
            mkNomi: mkNomi,
            mYili: mYili,
            sTime: sTime
        };

        if (edited >= 0) {
            cars[edited] = newCar;
            edited = -1;

        } else {
            cars.push(newCar);
        }

        drawList()
    } else {
        alert("Joyni to'ldiring!");
    }
}

function deleteCar(deletingIndex) {
    cars.splice(deletingIndex, 1);
    drawList();
}

function editCar(editedIndex) {
    document.forms['carForm']['countryName'].value = cars[editedIndex].dNomi;
    document.forms['carForm']['stateName'].value = cars[editedIndex].pNomi;
    document.forms['carForm']['carComName'].value = cars[editedIndex].mkNomi;
    document.forms['carForm']['carYear'].value = cars[editedIndex].mYili;
    document.forms['carForm']['setTime'].value = cars[editedIndex].sTime;
    edited = editedIndex;
}

function drawList() {
    document.getElementById('tBody').innerHTML = "";
    for (let i = 0; i < cars.length; i++) {
        document.getElementById('tBody').innerHTML += '<tr>' +
            '<td>' + cars[i].dNomi + '</td>' +
            '<td>' + cars[i].pNomi + '</td>' +
            '<td>' + cars[i].mkNomi + '</td>' +
            '<td>' + cars[i].mYili + '</td>' +
            '<td>' + cars[i].sTime.substr(11) + '</td>' +
            '<td>' + '<button class="btn btn-dark" onclick="editCar(' + i + ')">E</button>' +
            '</td>' +
            '<td>' + '<button class="btn btn-danger" onclick="deleteCar(' + i + ')">D</button>' +
            '</td>' +

            '</tr>';
    }
}



